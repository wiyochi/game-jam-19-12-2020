cmake_minimum_required(VERSION 3.10)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
set(CMAKE_CXX_EXTENSIONS OFF)

# SFML
add_subdirectory(SFML-2.5.1)
include_directories(${SFML_INCLUDE_DIR})