#include <game/Registry.hpp>

namespace game
{
	Registry::Registry()
	{
	}

	Registry::~Registry()
	{
		_enttRegistry.clear();
	}

	ObjectID_t Registry::createGameObject()
	{
		static ObjectID_t id = 0;

		entt::entity enttID = _enttRegistry.create();
		_idToEnttEntity.emplace(std::make_pair(id, enttID));

		return id++;
	}
}