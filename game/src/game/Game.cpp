#include <game/Game.hpp>

#include <systems/Physics.hpp>

#include <systems/Player.hpp>

#include <game/GameObject.hpp>

#include <entities/Player.hpp>

#include <components/Shape.hpp>
#include <components/Position.hpp>
#include <components/Rigidbody.hpp>
#include <components/PlayerController.hpp>

#include <spdlog/spdlog.h>

#include <resourcer/MapLoader.hpp>

#include <systems/ProjectileSystem.hpp>
#include <systems/EnemySystem.hpp>

#include <enemies/ProjectileEntity.hpp>
#include <enemies/EnemyEntity.hpp>


namespace game
{
	Game::Game() :
		_isRunning(false)
	{
	}

	Game::~Game()
	{
	}

	void Game::startup()
	{
		spdlog::set_level(spdlog::level::debug);

		_window = std::make_unique<sf::RenderWindow>(sf::VideoMode(800, 800), "Game");
		_world = std::make_shared<World>();

		_systems.push_back(std::make_unique<systems::EnemySystem>());
		_systems.push_back(std::make_unique<systems::ProjectileSystem>());
		_systems.push_back(std::make_unique<systems::Physics>());
		_systems.push_back(std::make_unique<systems::Player>());

		// Chargement de trucs concrets
		MapLoader::load();
		Rooms rooms(_world);

		_window->setView(sf::View(sf::Vector2f(0, 4900), sf::Vector2f(1200, 645)));

		// Create some entities to test

		std::shared_ptr<GameObject> floor = std::make_shared<GameObject>("Floor", _world);
		auto& pos = floor->addComponent<components::Position>(0.f, 600.f);
		auto& shape = floor->addComponent<components::Shape>(pos, Vec2(100.f, 10.f));
		floor->addComponent<components::Rigidbody>(_world->getPhysicWorld(), shape);

		auto player = std::make_shared<entities::Player>("Player", _world, Vec2(0, 4900));

		
		
		std::shared_ptr<DistanceEnemyEntity> enn = std::make_shared<DistanceEnemyEntity>(_world, Vec2(200.f, 200.f));

	}

	void Game::run()
	{
		if (_isRunning) return;
		_isRunning = true;

		float deltaTime = 0;

		while (_isRunning && _window->isOpen())
		{
			auto start = std::chrono::high_resolution_clock::now();

			sf::Event event;
			while (_window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					stop();
					return;
				}
			}

			// Updates
			for (auto& system : _systems)
			{
				system->update(deltaTime, _world);
			}

			std::this_thread::sleep_for(1ms); // simulate a bit of activity until we have some

			auto end = std::chrono::high_resolution_clock::now();
			deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			_window->clear();

			_world->getRegistry()->each<components::Shape>([this](components::Shape& shape)
			{
				_window->draw(shape);
			});

			_window->display();
		}
	}

	void Game::stop()
	{
		_isRunning = false;
		_window->close();
	}
}