#pragma once

#include <set>

#include <box2d/box2d.h>

#include <common.hpp>

#include <game/Registry.hpp>

namespace game
{
	class World
	{
	public:
		World();
		~World();

		World(const World&) = delete;
		World(World&&) = delete;

		ObjectID_t createGameObject();

		std::shared_ptr<b2World> getPhysicWorld() { return _b2World; }
		std::shared_ptr<Registry> getRegistry() { return _registry; }

	private:
		std::shared_ptr<Registry> _registry;
		std::shared_ptr<b2World> _b2World;
	};
}