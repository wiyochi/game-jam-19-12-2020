#pragma once

#include <common.hpp>

#include <game/World.hpp>

#include <components/Name.hpp>

namespace game
{
	class GameObject
	{
	public:
		GameObject(std::string const & name, std::shared_ptr<World> world);
		~GameObject();

		GameObject(GameObject const& other) = delete;
		GameObject(GameObject&& other) = delete;

		template<typename Component_t, typename ...Args>
		Component_t& addComponent(Args... args)
		{
			return _world->getRegistry()->addComponent<Component_t>(_id, std::forward<Args>(args)...);
		}

		template<typename Component_t>
		Component_t& getComponent()
		{
			return _world->getRegistry()->getComponent<Component_t>(_id);
		}

	private:
		std::shared_ptr<game::World> _world;

		uint64_t _id;
		components::Name& _nameComponent;
	};
}