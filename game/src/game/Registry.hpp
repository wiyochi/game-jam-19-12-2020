#pragma once

#include <map>

#include <entt/entt.hpp>

#include <common.hpp>

namespace game
{
	class Registry
	{
	public:
		Registry();
		~Registry();

		template<typename Component_t, typename ...Args>
		Component_t& addComponent(ObjectID_t id, Args... args)
		{
			return _enttRegistry.emplace<Component_t>(_idToEnttEntity[id], std::forward<Args>(args)...);
		}

		template<typename ...Components, typename FuncType>
		void each(FuncType function)
		{
			_enttRegistry.view<Components...>().each(function);
		}

		template<typename Component_t>
		Component_t& getComponent(ObjectID_t id)
		{
			return _enttRegistry.get<Component_t>(_idToEnttEntity[id]);
		}

		ObjectID_t createGameObject();

	private:
		entt::registry _enttRegistry;

		std::map<ObjectID_t, entt::entity> _idToEnttEntity;
	};
}