#pragma once

#include <SFML/Graphics.hpp>

#include <common.hpp>

#include <components/Rigidbody.hpp>
#include <components/Shape.hpp>

namespace components
{
	class PlayerController
	{
	public:
		enum Direction
		{
			Top,
			Right,
			Bottom,
			Left
		};

	public:
		PlayerController(float speed, Rigidbody& rigidbody, Shape& shape);
		~PlayerController();

		PlayerController(PlayerController&& other) noexcept;
		PlayerController& operator=(PlayerController&& other) noexcept;

		void movePlayer();
		bool isAttacking();
		sf::FloatRect getAttackZone();

	private:
		Direction _direction;
		float _playerSpeed;
		Rigidbody& _rigidbody;
		Shape& _shape;
	};
}