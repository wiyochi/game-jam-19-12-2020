#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <utils/Vec2.hpp>

#include <components/Rigidbody.hpp>


namespace components
{
    class Enemy
    {
        protected:
            float _speed;
            components::Rigidbody& _body;

        public:
            Enemy(Rigidbody& body):_body(body){}

            void move(const Vec2& pos, const float& speed)
            {
                //A*
                //TODO
                Vec2 playerPos(500.f, 500.f);
                Vec2 direction = (playerPos - pos);

                //vecteur unitaire
                direction *= 1/Vec2::distance(playerPos, pos);
                
                //vitesse
                direction = direction * speed;

                //Move toward player
                _body.setLinearVelocity(direction); 
            }

            virtual void attack(const float& deltaTime, std::shared_ptr<game::World> world, const components::Position& pos) {}
            
            Enemy(Enemy&& other) noexcept :
                _body(other._body)
            {
            }

            Enemy& operator=(Enemy&& other) noexcept
            {
                _body = other._body;

                return *this;
            }

    };
}

#endif