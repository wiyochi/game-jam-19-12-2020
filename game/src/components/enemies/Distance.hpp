#ifndef DISTANCE_HPP
#define DISTANCE_HPP

#include <components/enemies/Enemy.hpp>
#include <utils/Vec2.hpp>

#include <enemies/ProjectileEntity.hpp>
#include <components/Position.hpp>


namespace components
{

    class Distance : public Enemy
    {
        private:
            float _cdMax, _cd;
        public:
            Distance(components::Rigidbody& body)
            : Enemy(body), _cdMax(1000), _cd(0){}

            void attack(const float& deltaTime, std::shared_ptr<game::World> world, const components::Position& pos, const components::Shape& shapeComponent)
            {
                float ecart = 10.f;
                float speed = 10.f;
                
                //Shoot player
                if(_cd <= 0)
                {
                    _cd = _cdMax;

                    //new projectile
                    //get player pos
                    Vec2 playerPos(800.f, 200.f);
                    Vec2 playerShape(50.f, 50.f);

                    Vec2 playerCenter(playerPos.x + playerShape.x/2, playerPos.y + playerShape.y/2);

                    Vec2 shape = shapeComponent.getSize();

                    //Origine du projectile
                    Vec2 projStartPos(pos.x + shape.x/2, pos.y + shape.y/2);

                    if(playerCenter.x > pos.x + shape.x)
                    {
                        projStartPos.x = projStartPos.x + shape.x/2 + ecart;
                    }
                    else if(playerCenter.x < pos.x)
                    {
                        projStartPos.x = projStartPos.x - shape.x/2 - ecart;
                    }

                    if(playerCenter.y > pos.y + shape.y)
                    {
                        projStartPos.y = projStartPos.y + shape.y/2 + ecart;
                    }
                    else if(playerCenter.y < pos.y)
                    {
                        projStartPos.y = projStartPos.y - shape.y/2 - ecart;
                    }

                    //vitesse du projectile
                    Vec2 direction = (playerPos - pos);

                    //vecteur unitaire
                    direction *= 1/Vec2::distance(playerPos, pos);
                    
                    //vitesse
                    direction = direction * speed;

                    std::shared_ptr<ProjectileEntity> proj = std::make_shared<ProjectileEntity>(world, projStartPos, direction);
                }
                else
                {
                    _cd -= deltaTime;
                }
            }

            Distance(Distance&& other) noexcept :
                Enemy(other._body), _cdMax(other._cdMax), _cd(other._cd)
            {
            }

            Distance& operator=(Distance&& other) noexcept
            {
                _body = other._body;
                _cdMax = other._cdMax;
                _cd = other._cd;

                return *this;
            }

    };
}



#endif