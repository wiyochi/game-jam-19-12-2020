#ifndef PROJECTILE_HPP
#define PROJECTILE_HPP



#include <utils/Vec2.hpp>

#include <components/Rigidbody.hpp>


namespace components
{
    class Projectile
    {
        private:
            Vec2 _speed;
            components::Rigidbody& _body;

        public:
            Projectile(Rigidbody& body, const Vec2 speed): _body(body), _speed(speed){}

            void move()
            {
                _body.setLinearVelocity(_speed); 
            }

            Projectile(Projectile&& other) noexcept :
                _speed(other._speed),
                _body(other._body)
            {
            }

            Projectile& operator=(Projectile&& other) noexcept
            {
                _speed = other._speed;
                _body = other._body;

                return *this;
            }



    };
}

#endif