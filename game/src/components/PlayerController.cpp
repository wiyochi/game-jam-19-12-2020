#include <components/PlayerController.hpp>
#include <spdlog/spdlog.h>

namespace components
{
	PlayerController::PlayerController(float speed, Rigidbody& rigidbody, Shape& shape) :
		_playerSpeed(speed),
		_rigidbody(rigidbody),
		_shape(shape),
		_direction(Bottom)
	{
	}

	PlayerController::~PlayerController()
	{
	}

	PlayerController::PlayerController(PlayerController&& other) noexcept :
		_playerSpeed(other._playerSpeed),
		_rigidbody(other._rigidbody),
		_shape(other._shape),
		_direction(other._direction)
	{
	}

	PlayerController& PlayerController::operator=(PlayerController&& other) noexcept
	{
		_playerSpeed = other._playerSpeed;
		_rigidbody = other._rigidbody;
		_shape = other._shape;
		_direction = other._direction;

		return *this;
	}

	void PlayerController::movePlayer()
	{
		Vec2 vel;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			_direction = Top;
			vel.y -= _playerSpeed;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			_direction = Right;
			vel.x += _playerSpeed;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			_direction = Bottom;
			vel.y += _playerSpeed;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			_direction = Left;
			vel.x -= _playerSpeed;
		}

		_rigidbody.setLinearVelocity(vel);
	}

	bool PlayerController::isAttacking()
	{
		return sf::Keyboard::isKeyPressed(sf::Keyboard::Space);
	}

	sf::FloatRect PlayerController::getAttackZone()
	{
		sf::FloatRect attackZone;
		Vec2 pPos = _shape.getPosition();
		Vec2 pSize = _shape.getSize();
		Vec2 size(pSize.x + 40.f, 30.f);

		switch (_direction)
		{
		case Top:
			attackZone.top = pPos.y - size.y;
			attackZone.left = pPos.x - ((pPos.x - pSize.x) / 2);
			attackZone.width = size.x;
			attackZone.height = size.y;
			break;
		case Right:
			attackZone.top = pPos.y - ((size.x - pSize.x) / 2);
			attackZone.left = pPos.x + pSize.x;
			attackZone.width = size.y;
			attackZone.height = size.x;
			break;
		case Bottom:
			attackZone.top = pPos.y + pSize.y;
			attackZone.left = pPos.x + ((size.x - pSize.x) / 2);
			attackZone.width = size.x;
			attackZone.height = size.y;
			break;
		case Left:
			attackZone.top = pPos.y - ((size.x - pSize.x) / 2);
			attackZone.left = pPos.x - size.y;
			attackZone.width = size.y;
			attackZone.height = size.x;
			break;
		}
		return attackZone;
	}
}