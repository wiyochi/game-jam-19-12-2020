#include <components/Shape.hpp>

namespace components
{
	Shape::Shape(components::Position const& pos, Vec2 const& size) :
		_prectShape(std::make_shared<sf::RectangleShape>(sf::Vector2f(size.x, size.y)))
	{
		_prectShape->setPosition(sf::Vector2f(pos.x, pos.y));
		
	}

	Shape::~Shape()
	{
	}

	void Shape::showDebug(bool value)
	{
		if (value)
		{
			//_rectShape.setFillColor(sf::Color::Transparent);
			_prectShape->setOutlineColor(sf::Color::Green);
			_prectShape->setOutlineThickness(1);
		}
		else
		{
			_prectShape->setOutlineColor(sf::Color::Transparent);
			_prectShape->setOutlineThickness(0);
		}
	}

	void Shape::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(*_prectShape, states);
	}
}