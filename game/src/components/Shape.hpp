#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

#include <components/Position.hpp>
#include <utils/Vec2.hpp>

#include "spdlog/spdlog.h"
namespace components
{
	class Shape : public sf::Drawable
	{
	public:
		Shape(components::Position const& pos, Vec2 const& size);
		~Shape();

		void showDebug(bool value = true);

		void setSize(Vec2 const& size) { _prectShape->setSize(sf::Vector2f(size.x - 2, size.y - 2)); }
		void setPosition(Vec2 const& pos) { _prectShape->setPosition(sf::Vector2f(pos.x + 1, pos.y + 1)); }
		void setRotation(float angle) { _prectShape->setRotation(angle); }
		void setTexture(sf::Texture * texture) { _prectShape->setTexture(texture); }

		Vec2 getSize() const { return _prectShape->getSize(); };
		Vec2 getPosition() const { return _prectShape->getPosition(); }

	private:
		std::shared_ptr<sf::RectangleShape> _prectShape;
	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	};
}