#pragma once

#include <game/GameObject.hpp>
#include <components/Rigidbody.hpp>
#include <components/enemies/Distance.hpp>
#include <game/GameObject.hpp>

class DistanceEnemyEntity
{
    private:
        std::shared_ptr<game::GameObject> _distanceEnemy;

    public:
        DistanceEnemyEntity(std::shared_ptr<game::World> world, const Vec2& pos)
        {
            _distanceEnemy = std::make_shared<game::GameObject>("distanceEnemy", world);
            auto& cpos = _distanceEnemy->addComponent<components::Position>(pos.x, pos.y);
            auto& shape = _distanceEnemy->addComponent<components::Shape>(cpos, Vec2(50.f, 50.f));
            
            components::Rigidbody& body = _distanceEnemy->addComponent<components::Rigidbody>(world->getPhysicWorld(), shape, components::Rigidbody::Type::Dynamic);
            auto& tmp = _distanceEnemy->addComponent<components::Distance>(std::ref(body));

        }
};
