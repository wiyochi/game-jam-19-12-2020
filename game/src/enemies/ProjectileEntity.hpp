#pragma once

#include <game/GameObject.hpp>
#include <components/Rigidbody.hpp>
#include <components/enemies/Projectile.hpp>

class ProjectileEntity
{
    private:
        std::shared_ptr<game::GameObject> _proj;

    public:
        ProjectileEntity(std::shared_ptr<game::World> world, const Vec2& pos, const Vec2& speed)
        {
            _proj = std::make_shared<game::GameObject>("proj", world);
            auto& cpos = _proj->addComponent<components::Position>(pos.x, pos.y);
            auto& shape = _proj->addComponent<components::Shape>(cpos, Vec2(10.f, 10.f));
            
            components::Rigidbody& body = _proj->addComponent<components::Rigidbody>(world->getPhysicWorld(), shape, components::Rigidbody::Type::Dynamic);
            auto& projCompo = _proj->addComponent<components::Projectile>(std::ref(body), speed);   
        }
};
