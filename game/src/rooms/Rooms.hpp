#pragma once

#include <memory>
#include <vector>
#include <random>
#include "Room.hpp"

class Rooms
{
    static constexpr unsigned baseHeight = 100;
    std::shared_ptr<game::World> _world;
    std::vector<std::shared_ptr<Room>> _rooms;

    unsigned generateRoom(unsigned j, int x, int y)
    {
        const std::string textures_references = std::string(RESOURCES_ROOT_PATH) + std::string("rooms/room") + std::to_string(j) + std::string(".json");
        nlohmann::json datas;
        auto path = std::filesystem::path(textures_references);
        spdlog::debug("Loading {}.", std::filesystem::absolute(path).string());
        std::ifstream ifs(textures_references);
        ifs >> datas;

        unsigned width = datas["width"];
        unsigned height = datas["height"];
        std::string const & tiles = datas["tiles"];
        
        std::shared_ptr<Room> pr = std::make_shared<Room>(_world, x, y);
        for (unsigned i = 0; i < width * height; ++i)
        {
            //spdlog::debug("Add tile ({0};{1}) -> {2}.", i % width, i / width, tiles[i]);
            pr->addTile(i % width, baseHeight - i / width, tiles[i]);
        }
        _rooms.push_back(pr);

        return width;
    }
public:
    Rooms(std::shared_ptr<game::World> world): _world(world)
    {
        static constexpr unsigned roomNb = 20;
        unsigned dist = 0;
        std::mt19937 mt;
        std::uniform_int_distribution roomGen(1, 2);
        std::uniform_int_distribution distanceRoom(1, 5);
        for (unsigned i = 0; i < roomNb; ++i)
        {
            int id = roomGen(mt);
            dist += generateRoom(id, dist, 0);
            dist += distanceRoom(mt);
        }

        for (unsigned i = 0; i < dist; ++i)
        {
            auto tile = std::make_shared<game::GameObject>("Galerie", _world);
            auto pos = tile->addComponent<components::Position>(50.f * (i), 50.f * (baseHeight + 1));
            auto shape = tile->addComponent<components::Shape>(std::ref(pos), Vec2(50.f, 50.f));

            shape.setTexture(ResourcesManager::textures.at(' '));
            tile->addComponent<components::Rigidbody>(_world->getPhysicWorld(), std::ref(shape), components::Rigidbody::Type::Static);
        }
    }
};