#pragma once

#include <vector>
#include <game/GameObject.hpp>
#include <resourcer/ResourcesManager.hpp>
#include <components/Rigidbody.hpp>

#include "spdlog/spdlog.h"
class Room
{
    std::vector<std::shared_ptr<game::GameObject>> _tiles;
    std::shared_ptr<game::World> _world;

    unsigned _x, _y;

public:
    Room(std::shared_ptr<game::World> world, unsigned x, unsigned y):_world(world), _x(x), _y(y)
    {
    }

    void addTile(unsigned x, unsigned y, char c)
    {
        if (c == ' ') return;
        auto tile = std::make_shared<game::GameObject>("Wall", _world);
		auto pos = tile->addComponent<components::Position>(50.f * (x + _x), 50.f * (y + _y));
		auto shape = tile->addComponent<components::Shape>(std::ref(pos), Vec2(50.f, 50.f));

        shape.setTexture(ResourcesManager::textures.at(c));
        if (c == 'B')
            tile->addComponent<components::Rigidbody>(_world->getPhysicWorld(), std::ref(shape), components::Rigidbody::Type::Static);
    }
};