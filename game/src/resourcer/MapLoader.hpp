#pragma once

#include <fstream>
#include <filesystem>
#include <string>
#include <nlohmann/json.hpp>
#include "spdlog/spdlog.h"
#include "rooms/Room.hpp"
#include "rooms/Rooms.hpp"

#include <resourcer/ResourcesManager.hpp>

namespace MapLoader
{
    void load()
    {
        static const std::string textures_references = std::string(RESOURCES_ROOT_PATH) + std::string("textures/references.json");
        nlohmann::json datas;
        spdlog::set_level(spdlog::level::debug);
        //auto path = std::filesystem::path(textures_references);
        //spdlog::debug("Loading {0}.", std::filesystem::absolute(path).string());
        std::ifstream ifs(textures_references);
        ifs >> datas;

        for (auto& [key, value] : datas.items())
            ResourcesManager::loadTexture(key[0], value);
    }
};