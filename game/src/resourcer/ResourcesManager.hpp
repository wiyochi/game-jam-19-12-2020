#pragma once

#include <SFML/Graphics.hpp>
#include <map>
#include <memory>

namespace ResourcesManager
{
    using texture_p = sf::Texture *;
    extern std::map<char, texture_p> textures;

    void loadTexture(char key, std::string const & value);
};