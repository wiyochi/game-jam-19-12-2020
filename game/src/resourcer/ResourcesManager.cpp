#include "ResourcesManager.hpp"

#include "spdlog/spdlog.h"


std::map<char, ResourcesManager::texture_p> ResourcesManager::textures{};

void ResourcesManager::loadTexture(char key, std::string const & value)
{
    auto texture =  new sf::Texture();
    textures[key] = texture;
    if(!(textures[key]->loadFromFile(std::string(RESOURCES_ROOT_PATH) + std::string("textures/") + value)))
    {
		spdlog::debug("Error while loading texture {}.", value);
    }
  }