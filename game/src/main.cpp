#include <game/Game.hpp>

int main()
{
	game::Game myGame;
	myGame.startup();
	myGame.run();

	return 0;
}