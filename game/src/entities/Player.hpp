#pragma once

#include <SFML/Graphics.hpp>

#include <common.hpp>

#include <game/World.hpp>
#include <game/GameObject.hpp>

#include <components/Position.hpp>
#include <components/Rigidbody.hpp>
#include <components/PlayerController.hpp>
#include <components/Shape.hpp>

namespace entities
{
	class Player : public game::GameObject
	{
	public:
		Player(std::string const& name, std::shared_ptr<game::World> world, Vec2 const& startPos);
		~Player();

	private:
		components::Position& _position;
		components::Shape& _shape;
		components::Rigidbody& _rigidbody;
		components::PlayerController& _controller;
	};
}