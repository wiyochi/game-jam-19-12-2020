#include <entities/Player.hpp>

namespace entities
{
	Player::Player(std::string const& name, std::shared_ptr<game::World> world, Vec2 const & startPos) :
		game::GameObject(name, world),
		_position(addComponent<components::Position>(startPos)),
		_shape(addComponent<components::Shape>(std::ref(_position), Vec2(50.f, 50.f))),
		_rigidbody(addComponent<components::Rigidbody>(world->getPhysicWorld(), std::ref(_shape), components::Rigidbody::Type::Dynamic)),
		_controller(addComponent<components::PlayerController>(100, std::ref(_rigidbody), std::ref(_shape)))
	{
	}

	Player::~Player()
	{
	}
}