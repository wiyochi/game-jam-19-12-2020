#pragma once

#include <game/World.hpp>
#include <components/Rigidbody.hpp>
#include <components/enemies/Projectile.hpp>

#include <systems/System.hpp>

namespace systems
{
	class ProjectileSystem : public ISystem
	{
	public:
		void update(float deltaTime, std::shared_ptr<game::World> world)
        {
            world->getRegistry()->each<components::Projectile>([](components::Projectile& proj)
			{
                proj.move();
			});
        }
	};
}