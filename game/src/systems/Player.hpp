#pragma once

#include <common.hpp>

#include <systems/System.hpp>

namespace systems
{
	class Player : public ISystem
	{
	public:
		void update(float deltaTime, std::shared_ptr<game::World> world) override;

	private:

	};
}