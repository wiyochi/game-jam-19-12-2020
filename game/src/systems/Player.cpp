#include <systems/Player.hpp>

#include <game/World.hpp>

#include <components/PlayerController.hpp>
#include <components/Shape.hpp>

#include <spdlog/spdlog.h>

namespace systems
{
	void Player::update(float deltaTime, std::shared_ptr<game::World> world)
	{
		world->getRegistry()->each<components::PlayerController>([&world](components::PlayerController& controller)
			{
				controller.movePlayer();

				if (controller.isAttacking())
				{
					sf::FloatRect attackZone = controller.getAttackZone();
					// Changer le composant (genre au lieu de shape -> ennemy)
					world->getRegistry()->each<components::Shape>([&attackZone](components::Shape& shape)
						{
							if (attackZone.intersects(sf::FloatRect(shape.getPosition().toSfVector2f(), shape.getSize().toSfVector2f())))
								spdlog::debug("attack");// faire du trucs � shape
						});
				}
			});
	}
}