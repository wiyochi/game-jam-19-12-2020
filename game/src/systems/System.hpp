#pragma once

#include <game/World.hpp>

namespace systems
{
	class ISystem
	{
	public:
		virtual void update(float deltaTime, std::shared_ptr<game::World> world) = 0;
	};
}