#pragma once

#include <common.hpp>

#include <box2d/box2d.h>

#include <game/World.hpp>
#include <systems/System.hpp>

namespace systems
{
	class Physics : public ISystem
	{
	public:
		Physics();
		~Physics();

		void update(float deltaTime, std::shared_ptr<game::World> world) override;

	private:
		void smoothStates(std::shared_ptr<b2World> world);
		void resetSmoothStates(std::shared_ptr<b2World> world);
		void singleStep(float deltaTime, std::shared_ptr<b2World> world);

	private:
		static constexpr float FIXED_TIMESTEP = 1.f / 120.f;
		static constexpr int MAX_STEPS = 5;

		b2Vec2 _gravity;

		float _fixedTimestepAccumulator;
		float _fixedTimeStepAccumulatorRatio;
	};
}