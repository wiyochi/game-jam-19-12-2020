#pragma once

#include <game/World.hpp>
#include <components/Rigidbody.hpp>
#include <components/enemies/Distance.hpp>

#include <systems/System.hpp>

namespace systems
{
	class EnemySystem : public ISystem
	{
	public:
		void update(float deltaTime, std::shared_ptr<game::World> world)
        {
            world->getRegistry()->each<components::Position, components::Shape, components::Distance>([&](components::Position& pos, components::Shape& shape, components::Distance& distanceEnemy)
			{
                distanceEnemy.move(pos, 10.f);
                distanceEnemy.attack(deltaTime, world, pos, shape);
			});
        }
	};
}