#pragma once

#include <SFML/Graphics.hpp>
#include <box2d/box2d.h>

// vector / point
struct Vec2
{
	float x;
	float y;

	// Constructors
	Vec2() : x(0), y(0) {}

	Vec2(Vec2 const & other) : x(other.x), y(other.y) {}

	Vec2(Vec2&& other) noexcept : x(other.x), y(other.y) {}

	Vec2(float a, float b) : x(a), y(b) {}

	Vec2(sf::Vector2f const & sfmlVec) : x(sfmlVec.x), y(sfmlVec.y) {}

	Vec2(b2Vec2 const & b2Vec) : x(b2Vec.x), y(b2Vec.y) {}

	~Vec2() {}

	// Conversion to sfml vector
	sf::Vector2f toSfVector2f() const
	{
		return sf::Vector2f(x, y);
	}

	// Conversion to box2D vector
	b2Vec2 toB2Vec2() const
	{
		return b2Vec2(x, y);
	}

	// Get the distance between 2 points
	static float distance(Vec2 const& p1, Vec2 const& p2)
	{
		return std::sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
	}

	// Operators overloading
	Vec2& operator=(Vec2 const& other)
	{
		x = other.x; y = other.y;
		return *this;
	}

	Vec2& operator=(sf::Vector2f const& sfVec)
	{
		x = sfVec.x; y = sfVec.y;
		return *this;
	}

	Vec2& operator=(b2Vec2 const& b2Vec)
	{
		x = b2Vec.x; y = b2Vec.y;
		return *this;
	}

	Vec2 operator+(Vec2 const& other)
	{
		return Vec2(x + other.x, y + other.y);
	}

	Vec2 operator-(Vec2 const& other)
	{
		return Vec2(x - other.x, y - other.y);
	}

	Vec2 operator+(float f)
	{
		return Vec2(x + f, y + f);
	}

	Vec2 operator-(float f)
	{
		return Vec2(x - f, y - f);
	}

	Vec2 operator*(float m)
	{
		return Vec2(x * m, y * m);
	}

	Vec2& operator+=(Vec2 const& other)
	{
		x += other.x; y += other.y;
		return *this;
	}

	Vec2& operator-=(Vec2 const& other)
	{
		x -= other.x; y -= other.y;
		return *this;
	}

	Vec2& operator*=(float m)
	{
		x *= m; y *= m;
		return *this;
	}
};

inline bool operator==(Vec2 const& p1, Vec2 const& p2)
{
	return p1.x == p2.x && p1.y == p2.y;
}

inline bool operator!=(Vec2 const& p1, Vec2 const& p2)
{
	return !(p1 == p2);
}