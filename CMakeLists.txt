cmake_minimum_required(VERSION 3.10)

project(
    game
    VERSION 0.1.0
    DESCRIPTION "game"
    LANGUAGES CXX C
)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
set(CMAKE_CXX_EXTENSIONS OFF)

# Setup conan package manager
if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.15/conan.cmake" "${CMAKE_BINARY_DIR}/conan.cmake")
endif()
include("${CMAKE_BINARY_DIR}/conan.cmake")

conan_cmake_run(
    REQUIRES spdlog/1.8.0
             box2d/2.4.0
             entt/3.5.2
             nlohmann_json/3.9.1
    BASIC_SETUP CMAKE_TARGETS
    BUILD missing
)

set (RESPATH \"${PROJECT_SOURCE_DIR}/game/resources/\")
add_compile_definitions(RESOURCES_ROOT_PATH=${RESPATH})
message("Resource path : ${RESPATH}")

# Add extern folder for external libraries
add_subdirectory(extern)

# Add game code
add_subdirectory(game)
