# Game Jam du 19/12/2020
## On utilise :
 - [SFML](https://www.sfml-dev.org/ "Simple and Fast Multimedia Library")
 - [Box2D](https://box2d.org/ "A 2D Physics Engine for Games")
 - [spdlog](https://github.com/gabime/spdlog "Very fast, header-only/compiled, C++ logging library")
 - [entt](https://github.com/skypjack/entt "ECS in modern C++")
 - [nlohmann](https://github.com/nlohmann/json "JSON library")

## Il vous faut :
 - [CMake](https://cmake.org/ "Build")
 - [Conan](https://conan.io/ "C++ Packet manager")
 - [SFML](https://www.sfml-dev.org/download/sfml/2.5.1/index.php "Simple and Fast Multimedia Library")

## Comment c'est t'y qu'on fait ?
On clone le repo

    $ git clone git@gitlab.com:wiyochi/game-jam-19-12-2020.git
    $ cd game-jam-19-12-2020

On télécharge les sources [SFML](https://www.sfml-dev.org/download/sfml/2.5.1/index.php "Simple and Fast Multimedia Library") et on place le dossier _SFML-2.5.1_ dans le dossier _extern_. Ensuite, p'tit coup de _CMake_ et on est partis.

    $ mkdir build
    $ cd build
    $ cmake ..

Le reste, c'est selon votre bon vouloir (makefile / VS).

---
Les jameurs du confinement - Décembre 2020